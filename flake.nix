{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    attic.url = "github:zhaofengli/attic";
    flake-parts.url = "github:hercules-ci/flake-parts";
    sower.url = "git+https://git.junco.dev/adam/sower.git?ref=main";

    zmk-nix = {
      url = "github:lilyinstarlight/zmk-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    inputs@{ flake-parts, self, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } {
      imports = [ inputs.sower.flakeModules.seed ];

      systems = [
        "x86_64-linux"
        "aarch64-linux"
      ];

      perSystem =
        {
          inputs',
          lib,
          pkgs,
          ...
        }:
        {
          packages = rec {
            default = inputs'.nixpkgs.legacyPackages.linkFarm "zmk-config-default" [
              {
                name = "lotus58";
                path = lotus58;
              }
              {
                name = "lily58";
                path = lily58;
              }
            ];

            lily58 = inputs'.zmk-nix.legacyPackages.buildSplitKeyboard {
              name = "firmware";

              src = inputs.nixpkgs.lib.sourceFilesBySuffices self [
                "lily58.conf"
                "lily58.keymap"
                ".yml"
              ];

              board = "nice_nano";
              shield = "lily58_%PART%";

              zephyrDepsHash = "sha256-n7xX/d8RLqDyPOX4AEo5hl/3tQtY6mZ6s8emYYtOYOg=";

              meta = {
                description = "lotus58 firmware";
                license = inputs.nixpkgs.lib.licenses.mit;
                platforms = inputs.nixpkgs.lib.platforms.all;
              };
            };

            lotus58 = inputs'.zmk-nix.legacyPackages.buildSplitKeyboard {
              name = "firmware";

              src = inputs.nixpkgs.lib.sourceFilesBySuffices self [
                "lotus58.conf"
                "lotus58.keymap"
                ".yml"
              ];

              board = "nice_nano_v2";
              shield = "lotus58_%PART%";

              zephyrDepsHash = "sha256-n7xX/d8RLqDyPOX4AEo5hl/3tQtY6mZ6s8emYYtOYOg=";

              meta = {
                description = "lotus58 firmware";
                license = inputs.nixpkgs.lib.licenses.mit;
                platforms = inputs.nixpkgs.lib.platforms.all;
              };
            };

            update = inputs'.zmk-nix.packages.update;
          };

          devShells = {
            default = inputs'.zmk-nix.devShells.default;
            ci = pkgs.mkShellNoCC {
              packages = [
                inputs'.attic.packages.attic
                inputs'.sower.packages.seed-ci
              ];
            };
          };
        };
    };
}
